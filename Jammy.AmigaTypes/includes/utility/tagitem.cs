namespace Jammy.AmigaTypes;

public class TagItem
{
	public Tag ti_Tag { get; set; }
	public ULONG ti_Data { get; set; }
}

