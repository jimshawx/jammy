namespace Jammy.AmigaTypes;

public class _Object
{
	public MinNode o_Node { get; set; }
	public IClassPtr o_Class { get; set; }
}

