﻿using Jammy.Core.Types.Types;
using Jammy.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

// ReSharper disable InconsistentNaming

/*
	Copyright 2020-2021 James Shaw. All Rights Reserved.
*/

namespace Jammy.Disassembler
{
	public class Disassembler
	{
		private StringBuilder asm;
		private uint pc;
		private byte[] memory;
		private uint address;
		private DAsm dasm;

		public DAsm Disassemble(uint add, IEnumerable<byte> m)
		{
			try
			{
				dasm = new DAsm();
				memory = m.ToArray();
				//memoryE = m;
				pc = 0;
				this.address = add;
				asm = new StringBuilder();
				ushort ins = read16(pc);
				pc += 2;

				int type = (int)(ins >> 12);

				switch (type)
				{
					case 0:
					case 1:
					case 2:
					case 3:
						t_zero(ins);
						break;
					case 4:
						t_four(ins);
						break;
					case 5:
						t_five(ins);
						break;
					case 6:
						t_six(ins);
						break;
					case 7:
						t_seven(ins);
						break;
					case 8:
						t_eight(ins);
						break;
					case 9:
						t_nine(ins);
						break;
					case 11:
						t_eleven(ins);
						break;
					case 12:
						t_twelve(ins);
						break;
					case 13:
						t_thirteen(ins);
						break;
					case 14:
						t_fourteen(ins);
						break;
					case 15:
						t_fifteen(ins);
						break;
					default:
						Append($"unknown_instruction_{ins:X4}");
						break;
				}

				if (pc > memory.Length)
				{
					dasm.Asm = "unknown";
					dasm.Bytes = memory.Length >= 2 ? memory : new byte[] {0, 0};
					dasm.Address = address;
				}
				else
				{
					dasm.Asm = asm.ToString();
					dasm.Bytes = memory[0..(int)pc];
					//dasm.Bytes = memoryE.Take((int)pc).ToArray();
					dasm.Address = address;
				}

				return dasm;
			}
			catch (Exception ex)
			{
				var dasm = new DAsm();
				dasm.Asm = ex.ToString();
				dasm.Bytes = new byte[] {0, 0};
				dasm.Address = address;
				return dasm;
			}
		}

		private void Append(string s)
		{
			asm.Append(s);
		}

		private void Append(Size s)
		{
			if (s == Size.Byte)	asm.Append(".b ");
			else if (s == Size.Word) asm.Append(".w ");
			else if (s == Size.Long) asm.Append(".l ");
		}

		private void AppendBcc(Size s)
		{
			if (s == Size.Byte) asm.Append(".s ");
			else if (s == Size.Word) asm.Append(".w ");
			else if (s == Size.Long) asm.Append(".l ");
		}

		private string fmtX2(uint x)
		{
			x &= 0xff;
			if (x < 10)
				return string.Format($"{x}");
			else if (x < 16)
				return string.Format($"${x:X1}");
			return string.Format($"${x:X2}");
		}

		private string fmtX4(uint x)
		{
			x &= 0xffff;

			if (x < 10)
				return string.Format($"{x}");
			else if (x < 16)
				return string.Format($"${x:X1}");
			else if (x < 256)
				return string.Format($"${x:X2}");
			return string.Format($"${x:X4}");
		}

		private string fmtX4o(uint x)
		{
			x &= 0xffff;

			if ((short) x < 0)
			{
				return string.Format($"{(short)x}");
			}
			else
			{
				if (x < 10)
					return string.Format($"{x}");
				else if (x < 16)
					return string.Format($"${x:X1}");
				else if (x < 256)
					return string.Format($"${x:X2}");
				return string.Format($"${x:X4}");
			}
		}

		private string fmtX8(uint x)
		{
			if (x < 10)
				return string.Format($"{x}");
			else if (x < 16)
				return string.Format($"${x:X1}");
			else if (x < 256)
				return string.Format($"${x:X2}");
			else if (x < 65536)
				return string.Format($"${x:X4}");
			else if (x < 0x1000000)
				return string.Format($"${x:X6}");
			return string.Format($"${x:X8}");
		}

		private string fmt(uint x, Size s)
		{
			if (s == Size.Byte) return fmtX2(x);
			if (s == Size.Word) return fmtX4(x);
			if (s == Size.Long) return fmtX8(x);
			return $"unknown_size_{s}";
		}

		private void Append(uint imm, Size s)
		{
			asm.Append(fmt(imm,s));
		}

		private void Remove(int count)
		{
			asm.Length-=count;
		}

		private uint read32(uint addr)
		{
			if (addr + 3 >= memory.Length) return 0;
			return ((uint)memory[addr] << 24) +
					((uint)memory[addr + 1] << 16) +
					((uint)memory[addr + 2] << 8) +
					(uint)memory[addr + 3];
			//var mm = memoryE.Skip((int)addr).Take(4);
			//return mm.Aggregate<byte, uint>(0, ( curr, next) => (curr<<8)|next);
		}

		private ushort read16(uint addr)
		{
			if (addr + 1 >= memory.Length) return 0;
			return (ushort)(
				((ushort)memory[addr] << 8) +
				(ushort)memory[addr + 1]);
			//var mm = memoryE.Skip((int)addr).Take(2);
			//return mm.Aggregate<byte, ushort>(0, (curr, next) => (ushort)((curr << 8) | next));
		}

		private byte read8(uint addr)
		{
			if (addr >= memory.Length) return 0;
			return memory[addr];
			//var mm = memoryE.Skip((int)addr);
			//return mm.FirstOrDefault();
		}

		private uint fetchEA(int type)
		{
			int m = (type >> 3) & 7;
			int x = type & 7;

			switch (m)
			{
				case 0:
					//return d[x];
					Append($"d{x}");
					return 0;
				case 1:
					//return a[x];
					if (x == 7)
						Append("sp");
					else
						Append($"a{x}");
					return 0;
				case 2:
					//return a[x];
					if (x == 7)
						Append("(sp)");
					else
						Append($"(a{x})");
					return 0;
				case 3:
					if (x == 7)
						Append("(sp)+");
					else
						Append($"(a{x})+");
					return 0;
				//					return a[x];
				case 4:
					if (x == 7)
						Append("-(sp)");
					else
						Append($"-(a{x})");
					return 0;
				//return a[x];
				case 5://(d16,An)
					{
						ushort d16 = read16(pc);
						pc += 2;
						//return a[x] + (uint)(short)d16;
						Append($"{fmtX4o(d16)}(a{x})");
						return 0;

					}
				case 6://(d8,An,Xn)
					{
						uint ext = read16(pc); pc += 2;
						uint Xn = (ext >> 12) & 7;
						uint d8 = ext & 0xff;
						uint scale = (ext >>9)&3;
						string ss = scale == 0 ? "" : $"*{1<<(int)scale}";
						string s = (((ext>>11)&1) != 0)?"l":"w";
						if ((ext & 0x8000)!=0)
							Append($"{fmtX2(d8)}(a{x},a{Xn}.{s}{ss})");
						else
							Append($"{fmtX2(d8)}(a{x},d{Xn}.{s}{ss})");
						return 0;
					}
				case 7:
					switch (x)
					{
						case 0b010://(d16,pc)
							{
								ushort d16 = read16(pc);
								uint d32 = (uint)(address + pc + (short)d16);
								dasm.ea = d32;
								pc += 2;
								Append($"{fmtX8(d32)}(pc)");
								return 0;
							}
						case 0b011://(d8,pc,Xn)
							{
								uint ext = read16(pc);
								uint Xn = (ext >> 12) & 7;
								uint d8 = ext & 0xff;
								uint scale = (ext >> 9) & 3;
								string ss = scale == 0 ? "" : $"*{1 << (int)scale}";
								string s = (((ext >> 11) & 1) != 0) ? "l" : "w";
								d8 += (address + pc);
								pc += 2;
								if ((ext&0x8000)!=0)
									Append($"{fmtX8(d8)}(a{Xn}.{s}{ss})");
								else
									Append($"{fmtX8(d8)}(d{Xn}.{s}{ss})");
								return 0;
							}
						case 0b000://(xxx).w
							{
								uint ea = (uint)(short)read16(pc);
								dasm.ea = ea;
								pc += 2;
								Append($"{fmtX4(ea)}");
								return ea;
							}
						case 0b001://(xxx).l
							{
								uint ea = read32(pc);
								dasm.ea = ea;
								pc += 4;
								Append($"{fmtX8(ea)}");
								return ea;
							}
						case 0b100: //#imm
							//Append($"{fmtX8(address + pc)}");
							return pc;
						default:
							Append($"unknown_effective_address_mode_{type:X4}");
							return 0;
					}
			}

			Append($"unknown_effective_address_mode_{type:X4}");
			return 0;
		}

		private uint fetchOpSize(uint ea, Size size)
		{
			//todo: trap on odd aligned access
			if (size == Size.Long)
				return read32(ea);
			if (size == Size.Word)
				return (uint)(short)read16(ea);
			if (size == Size.Byte)
				return (uint)(sbyte)read8(ea);
			Append("unknown_size");
			return 0;
		}

		private double fetchImmFP(FPSize size)
		{
			double v = 0.0;

			if (size == FPSize.Single)
			{	
				uint b0 = read32(pc);
				pc += 4;
				try
				{ 
					v = (double)BitConverter.UInt32BitsToSingle(b0);
				}
				catch
				{
					Append($"#${b0:X8}");
					return 0.0;
				}
			}
			else if (size == FPSize.Double)
			{
				ulong b0 = read32(pc);
				ulong b1 = read32(pc + 4);
				pc += 8;
				try
				{ 
					v = BitConverter.UInt64BitsToDouble((b0<<32)|b1);
				}
				catch
				{
					Append($"#${b0:X8}{b1:X8}");
					return 0.0;
				}
			}
			else if (size == FPSize.Extended)
			{
				ulong b0 = read32(pc);
				ulong b1 = read32(pc+4);
				ulong b2 = read16(pc+8);
				pc += 10;
				//todo: enough bits?
				try
				{
					v = BitConverter.UInt64BitsToDouble((b0<< 32) | b1);
					v += b2/(double)Math.Pow(2.0,53-16);
				}
				catch
				{
					Append($"#${b0:X8}{b1:X8}{b2:X4}");
					return 0.0;
				}
			}
			else if (size == FPSize.Packed)
			{
				ulong b0 = read32(pc);
				ulong b1 = read32(pc + 4);
				ulong b2 = read32(pc + 8);
				pc += 12;
				//1 bit sign exponent
				//1 bit size mantissa
				//2 bits for Nan
				//11 bits exponent
				//13 bits zero
				//68 bits mantissa
				//todo: how is this really decoded?
				Append($"#${b0:X8}{b1:X8}{b2:X8}");
			}
			Append($"#{v}");
			return v;
		}

		private uint fetchImm(Size size)
		{
			uint v = 0;
			if (size == Size.Long)
			{
				v = fetchOpSize(pc, size);
				pc += 4;
				Append($"#{fmtX8(v)}");
			}
			else if (size == Size.Word)
			{
				v = fetchOpSize(pc, size);
				pc += 2;
				Append($"#{fmtX4(v)}");
			}
			else if (size == Size.Byte)
			{
				//immediate bytes are stored in a word
				v = fetchOpSize(pc, Size.Word);
				v = (uint)(sbyte)v;
				pc += 2;
				Append($"#{fmtX2(v)}");
			}
			else
			{
				Append("unknown_immediate_size");
			}
			return v;
		}

		private double fetchOpFP(int type, uint ea, FPSize size)
		{
			if (size == FPSize.Long) return fetchOp(type, ea, Size.Long);
			if (size == FPSize.Word) return fetchOp(type, ea, Size.Word);
			if (size == FPSize.Byte) return fetchOp(type, ea, Size.Byte);
			int m = (type >> 3) & 7;
			int x = type & 7;
			if (m != 7 || x != 4)
			{ 
				return 0;
			}
			return fetchImmFP(size);
		}

		private uint fetchOp(int type, uint ea, Size size)
		{
			int m = (type >> 3) & 7;
			int x = type & 7;

			switch (m)
			{
				//	case 0:
				//		return ea;

				//	case 1:
				//		return ea;

				//	case 2:
				//		return fetchOpSize(ea, size);

				//	case 3:
				//		{
				//			uint v = fetchOpSize(ea, size);
				//			if (size == Size.Long)
				//				a[x] += 4;
				//			else if (size == Size.Word)
				//				a[x] += 2;
				//			else if (size == Size.Byte)
				//				a[x] += 1;
				//			return v;
				//		}

				//	case 4:
				//		{
				//			if (size == Size.Long)
				//				a[x] -= 4;
				//			else if (size == Size.Word)
				//				a[x] -= 2;
				//			else if (size == Size.Byte)
				//				a[x] -= 1;
				//			return fetchOpSize(a[x], size);//yes, a[x]
				//		}

				//	case 5://(d16,An)
				//		return fetchOpSize(ea, size);

				//	case 6://(d8,An,Xn)
				//		return fetchOpSize(ea, size);

				case 7:
					switch (x)
					{
						case 0b010://(d16,pc)
							return fetchOpSize(ea, size);
						case 0b011://(d8,pc,Xn)
							return fetchOpSize(ea, size);
						case 0b000://(xxx).w
							return ea;//fetchOpSize(ea, size);
						case 0b001://(xxx).l
							return ea;//fetchOpSize(ea, size);
						case 0b100://#imm
							uint imm = fetchImm(size);//ea==pc
							return imm;
						default:
							Append($"unknown_effective_address_mode_{type:X4}");
							return 0;
					}
			}
			return 0;
		}

		private Size getSize(int type)
		{
			int s = (type >> 6) & 3;
			if (s == 0) { Append(".b "); return Size.Byte; }
			if (s == 1) { Append(".w "); return Size.Word; }
			if (s == 2) { Append(".l "); return Size.Long; }
			Append($".unknown ");
			return (Size)3;
		}

		private readonly Tuple<uint, string>[] fpconst = [
			new Tuple<uint, string>(0x00 ,"pi"),
			new Tuple<uint, string>(0x0B ,"Log10(2),"),
			new Tuple<uint, string>(0x0C ,"e"),
			new Tuple<uint, string>(0x0D ,"Log2(e),"),
			new Tuple<uint, string>(0x0E ,"Logl0(e),"),
			new Tuple<uint, string>(0x0F ,"0.0"),
			new Tuple<uint, string>(0x30 ,"ln(2)"),
			new Tuple<uint, string>(0x31 ,"ln(10)"),
			new Tuple<uint, string>(0x32 ,"10^0"),
			new Tuple<uint, string>(0x33 ,"10^1"),
			new Tuple<uint, string>(0x34 ,"10^2"),
			new Tuple<uint, string>(0x35 ,"10^4"),
			new Tuple<uint, string>(0x36 ,"10^8"),
			new Tuple<uint, string>(0x37 ,"10^16"),
			new Tuple<uint, string>(0x38 ,"10^32"),
			new Tuple<uint, string>(0x39 ,"10^64"),
			new Tuple<uint, string>(0x3A ,"10^128"),
			new Tuple<uint, string>(0x3B ,"10^256"),
			new Tuple<uint, string>(0x3C ,"10^512"),
			new Tuple<uint, string>(0x3D ,"10^1024"),
			new Tuple<uint, string>(0x3E ,"10^2048"),
			new Tuple<uint, string>(0x3F ,"10^4096")
		];

		private string GetFPConst(int cc)
		{
			return fpconst.Single(x => x.Item1 == (uint)cc).Item2;
		}

		private readonly Tuple<string, uint>[] fpcc = [
			new Tuple<string,uint>("EQ"    ,0b000001),
			new Tuple<string,uint>("NE"    ,0b001110),
			new Tuple<string,uint>("GT"    ,0b010010),
			new Tuple<string,uint>("NGT"   ,0b011101),
			new Tuple<string,uint>("GE"    ,0b010011),
			new Tuple<string,uint>("NGE"   ,0b011100),
			new Tuple<string,uint>("LT"    ,0b010100),
			new Tuple<string,uint>("NLT"   ,0b011011),
			new Tuple<string,uint>("LE"    ,0b010101),
			new Tuple<string,uint>("NLE"   ,0b011010),
			new Tuple<string,uint>("GL"    ,0b010110),
			new Tuple<string,uint>("NGL"   ,0b011001),
			new Tuple<string,uint>("GLE"   ,0b010111),
			new Tuple<string,uint>("NGLE"  ,0b011000),

			new Tuple<string,uint>("OGT"   ,0b000010),
			new Tuple<string,uint>("ULE"   ,0b001101),
			new Tuple<string,uint>("OGE"   ,0b000011),
			//new Tuple<string,uint>("ULT"   ,0b001101),//same as ULE so documentation I have must be wrong
			new Tuple<string,uint>("OLT"   ,0b000100),
			new Tuple<string,uint>("UGE"   ,0b001011),
			new Tuple<string,uint>("OLE"   ,0b000101),
			new Tuple<string,uint>("UGT"   ,0b001010),
			new Tuple<string,uint>("OGL"   ,0b000110),
			new Tuple<string,uint>("UEQ"   ,0b001001),

			new Tuple<string,uint>("F"     ,0b000000),
			new Tuple<string,uint>("T"     ,0b001111),
			new Tuple<string,uint>("SF"    ,0b010000),
			new Tuple<string,uint>("ST"    ,0b011111),
			new Tuple<string,uint>("SEQ"   ,0b010001),
			new Tuple<string,uint>("SNE"   ,0b011110)
		];

		private string GetFPCC(int cc)
		{
			var ccex = fpcc.SingleOrDefault(x => x.Item2 == (uint)cc);
			if (ccex == null)
			{
				Append($"unknown_FPCC_{cc}_{cc:X2}");
				return string.Empty;
			}
			return ccex.Item1.ToLower();
		}

		private void t_fifteen(int type)
		{
			int misc = (type>>6)&7;
			if (misc != 0)
			{
				switch (misc)
				{
					case 0b010://FBcc or fnop (and ea is $+2.w)
					case 0b011://FBcc
						{ 
						var size = (type>>6)&1;
						var cc = type&0x3f;
						if (size == 0 && cc == 0 && read16(pc)==pc+2)
						{ 
							Append("fnop");
							break;
						}
						Append("fb");
						Append($"{GetFPCC(cc)}");
						if (size == 1)
						{
							Append(".l ");
							uint ea = (uint)(int)read32(pc); pc += 4;
							Append($"#{fmtX8(ea+address+2)}");
						}
						else
						{
							Append(".w ");
							uint ea = (uint)(short)read16(pc); pc += 2;
							Append($"#{fmtX4(ea+address+2)}");
						}
						}
						break;
					case 0b101:
						{ 
							Append("frestore");
							Append(" ");
							Size size = getSize(type);
							uint ea = fetchEA(type);
							fetchOp(type, ea, size);
						}
						break;
					case 0b100:
						{ 
							Append("fsave");
							Append(" ");
							Size size = getSize(type);
							uint ea = fetchEA(type);
							fetchOp(type, ea, size);
						}
						break;
					case 0b001://FDBcc or FScc or FTRAPcc
						{
							var w2 = read16(pc); pc += 2;
							var cc = w2&0x3f;
							var ins = (type >> 3) & 7;
							if (ins == 0b001)
							{
								var dr = type & 7;
								Append($"fdb{GetFPCC(cc)} ");
								Append($"d{dr},");
								uint ea = (uint)(short)read16(pc); pc += 2;
								Append($"#{fmtX4(ea + address + 2)}");
							}
							else if (ins == 0xb111)
							{
								Append($"ftrap{GetFPCC(cc)}");
								var mode = type & 7;
								if (mode == 0b010)
								{
									Append($".w #{fmtX4(read16(pc))}"); pc += 2;
								}
								else if (mode == 0b011)
								{
									Append($".l #{fmtX8(read32(pc))}"); pc += 4;
								}
								else if (mode != 0b100)
								{
									Append("unknown_mode");
								}
							}
							else
							{ 
								Append($"fs{GetFPCC(cc)}.b ");
								uint ea = fetchEA(type);
								fetchOp(type, ea, Size.Byte);
							}
						}
						break;
				}
			}
			else
			{ 
				int ext = read16(pc); pc += 2;

				if ((type & 0b11_1111_1111) == 0 && (ext >> 12) == 0b010111)
				{
					int cc = ext&0x7f;
					Append($"fmovecr.x #${cc:X2},fp{(ext>>7)&7}  ; {GetFPConst(cc)}");
					return;
				}
				else if ((ext & 0b11_000_111_00000000) == 0b11_000_000_00000000)
				{
					Append("fmovem.x ");

					int mode = (ext >> 10) & 3;

					if (((ext>>13)&1)==0)//M->R
					{
						Size size = (Size)((type >> 6) & 3);
						uint ea = fetchEA(type);
						fetchOp(type, ea, size);
						Append(",");
					}

					var list = ext & 0xff;
					if (mode == 0 || mode == 1)
					{
						//pre-decrement is backwards
						for (int i = 0; i < 4; i++)
						{
							int b0 = list&(1<<i);
							int b7 = list&(0x80>>i);
							list &= 0xff-(1<<i)-(0x80>>i);
							list |= b7>>(7-i);
							list |= b0<<(7-i);
						}
					}
					if (mode == 1 || mode == 3)
					{
						if (mode == 0 || mode == 1) Append("-");
						Append($"(a{(ext>>4)&7})");
						if (mode == 2 || mode == 3) Append("+");
					}
					else
					{ 
						var ls = list << 1;
						bool dash = false;
						bool slash = false;
						for (int i = 0; i < 8; i++)
						{
							if ((ls & 3) == 0b010) { if (slash) Append("/"); Append($"fp{i}"); slash = true; }
							if ((ls & 7) == 0b111) { if (!dash) { Append("-"); dash = true; } }
							if ((ls & 6) == 0b010) { if (dash) Append($"fp{i}"); dash = false; }
							if ((ls & 15) == 0b0110) { Append($"/fp{i + 1}"); }
							ls >>= 1;
						}
					}

					if (((ext >> 13) & 1) == 1)//R->M
					{
						Append(",");
						Size size = (Size)((type >> 6) & 3);
						uint ea = fetchEA(type);
						fetchOp(type, ea, size);
					}

					return;
				}

				bool rm = ((ext>>14)&1)!=0;
				int ss = (ext>>10)&7;
				int dr = (ext>>7)&7;
			
				int ins = ext&0x7f;
				if ((ins & 0b1111_000) == 0b0110_000)
					Append("fsincos");
				switch (ins)
				{
					case 0b0011000:	Append("fabs"); break;
					case 0b0011100: Append("facos"); break;
					case 0b0100010: Append("fadd"); break;
					case 0b0001100: Append("fasin"); break;
					case 0b0001010: Append("fatan"); break;
					case 0b0001101: Append("fatanh"); break;
					case 0b0111000: Append("fcmp"); break;
					case 0b0011101: Append("fcos"); break;
					case 0b0011001: Append("fcosh"); break;
					case 0b0100000: Append("fdiv"); break;
					case 0b0010000: Append("fetox"); break;
					case 0b0001000: Append("fetoxm1"); break;
					case 0b0011110: Append("fgetexp"); break;
					case 0b0011111: Append("fgetman"); break;
					case 0b0000001: Append("fint"); break;
					case 0b0000011: Append("fintrz"); break;
					case 0b0010101: Append("flog10"); break;
					case 0b0010110: Append("flog2"); break;
					case 0b0010100: Append("flogn"); break;
					case 0b0000110: Append("flognp1"); break;
					case 0b0100001: Append("fmod"); break;
					case 0b0000000: Append("fmove"); break;
					case 0b0100011: Append("fmul"); break;
					case 0b0011010: Append("fneg"); break;
					case 0b0100101: Append("frem"); break;
					case 0b0100110: Append("fscale"); break;
					case 0b0100100: Append("fsgldiv"); break;
					case 0b0100111: Append("fsglmul"); break;
					case 0b0001110: Append("fsin"); break;
					case 0b0000010: Append("fsinh"); break;
					case 0b0000100: Append("fsqrt"); break;
					case 0b0101000: Append("fsub"); break;
					case 0b0001111: Append("ftan"); break;
					case 0b0001001: Append("ftanh"); break;
					case 0b0010010: Append("ftentox"); break;
					case 0b0111010: Append("ftst"); break;
					case 0b0010001: Append("ftwotox"); break;
				}
				//fmove has some special cases
				if (ins == 0b0000000)
				{
					if ((ext>>14)==0b10)//100,101
					{
						Append(".l ");
						string cr = ss==0b001?"fpiar":(ss==0b010?"fpsr":"fpcr");
						if (rm)
						{
							Append($"{cr},");
							uint ea = fetchEA(type);
							fetchOpFP(type, ea, (FPSize)ss);
						}
						else
						{
							uint ea = fetchEA(type);
							fetchOpFP(type, ea, (FPSize)ss);
							Append($",{cr}");
						}
						return;
					}
					if ((ext>>13)==0b010 || (ext >> 13) == 0b000)
					{ 
						if (!rm)
						{
							//reg to reg
							Append(".x ");
							Append($"fp{ss},");
						}
						else
						{
							//ea to reg
							string sizes = "lsxpwdb?";
							Append($".{sizes[ss]} ");
							uint ea = fetchEA(type);
							fetchOpFP(type, ea, (FPSize)ss);
							Append(",");
						}
						Append($"fp{dr}");
						return;
					}
					if ((ext >> 13) == 0b011)
					{
						//reg to ea
						string sizes = "lsxpwdbp";
						Append($".{sizes[ss]} ");
						Append($"fp{dr},");
						uint ea = fetchEA(type);
						fetchOpFP(type, ea, (FPSize)ss);
						int k = ((sbyte)((ext&0x7f)<<1))>>1;
						if (ss == 3)
							Append($",{k}");
						else if (ss==7)
							Append($",d{(ext>>4)&7}");
						return;
					}
				}

				if (rm)
				{
					string sizes = "lsxpwdb?";
					Append($".{sizes[ss]} ");
					uint ea = fetchEA(type);
					fetchOpFP(type, ea, (FPSize)ss);
					Append(",");
					if ((ins & 0b1111_000) == 0b0110_000)
						Append($"fp{ins & 7}:");
					Append($"fp{dr}");
				}
				else
				{
					Append(".x ");
					if (ss != dr)
						Append($"fp{ss},");
					if ((ins & 0b1111_000) == 0b0110_000)
						Append($"fp{ins & 7}:");
					Append($"fp{dr}");
				}
			}
		}

		private void t_fourteen(int type)
		{
			int mode = (type & 0b11_000_000) >> 6;
			int lr = (type & 0b1_00_000_000) >> 8;

			if (mode == 3)
			{
				int op = (type & 0b111_000_000_000)>>9;
				string rots = "#1";
				switch (op)
				{
					case 0: asd(type, rots, lr, Size.Word); break;
					case 1: lsd(type, rots, lr, Size.Word); break;
					case 2: roxd(type, rots, lr, Size.Word); break;
					case 3: rod(type, rots, lr, Size.Word); break;
					default: Append($"unknown_instruction_{type:X4}"); break;
				}
			}
			else
			{
				int op = (type & 0b11_000) >> 3;
				int rot = (type & 0b111_0_00_0_00_000) >> 9;
				string rots;

				if ((type & 0b1_00_000) != 0)
				{
					//rot = (int)(d[rot] & 0x3f);
					rots = $"d{rot}";
				}
				else
				{
					if (rot == 0) rot = 8;
					rots = $"#{rot}";
				}

				Size size = getSize(type);
				Remove(3);

				//EA is d[x]
				type &= 0b1111111111000111;

				switch (op)
				{
					case 0: asd(type, rots, lr, size); break;
					case 1: lsd(type, rots, lr, size); break;
					case 2: roxd(type, rots, lr, size); break;
					case 3: rod(type, rots, lr, size); break;
				}
			}
		}

		private void rod(int type, string rot, int lr, Size size)
		{
			if (lr == 1)
			{
				Append($"rol");
				Append(size);
				Append(rot);
			}
			else
			{
				Append($"ror");
				Append(size);
				Append(rot);
			}
			Append(",");
			uint ea = fetchEA(type);
			uint val = fetchOp(type, ea, size);
		}

		private void roxd(int type, string rot, int lr, Size size)
		{
			if (lr == 1)
			{
				Append($"roxl");
				Append(size);
				Append(rot);
			}
			else
			{
				Append($"roxr");
				Append(size);
				Append(rot);
			}
			Append(",");

			uint ea = fetchEA(type);
			uint val = fetchOp(type, ea, size);
		}

		private void lsd(int type, string rot, int lr, Size size)
		{
			if (lr == 1)
			{
				Append($"lsl");
				Append(size);
				Append(rot);
			}
			else
			{
				Append($"lsr");
				Append(size);
				Append(rot);
			}
			Append(",");
			uint ea = fetchEA(type);
			uint val = fetchOp(type, ea, size);
		}

		private void asd(int type, string rot, int lr, Size size)
		{
			if (lr == 1)
			{
				Append($"asl");
				Append(size);
				Append(rot);
			}
			else
			{
				Append($"asr");
				Append(size);
				Append(rot);
			}
			Append(",");
			uint ea = fetchEA(type);
			int val = (int)fetchOp(type, ea, size);
		}

		private void t_thirteen(int type)
		{
			//add

			int s = (type >> 6) & 3;
			Size size = 0;
			if (s == 3)
			{
				Append("adda");
				//adda
				if ((type & 0b1_00_000_000) != 0)
					size = Size.Long;
				else
					size = Size.Word;

				Append(size);

				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);

				int Xn = (type >> 9) & 7;
				Append($",a{Xn}");
			}
			else if ((type & 0b1_00_110_000) == 0b1_00_000_000)
			{
				Append("addx");

				if (s == 0) size = Size.Byte;
				else if (s == 1) size = Size.Word;
				else if (s == 2) size = Size.Long;
				Append(size);
				//addx
				int Xn = (type >> 9) & 7;

				if ((type & 0b1_000) != 0)
				{
					//M->M
					Append($"-(a{Xn}),");
					type ^= 0b101_000;
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
				}
				else
				{
					//R->R
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
					Append($",d{Xn}");
				}
			}
			else
			{
				Append("add");
				if (s == 0) size = Size.Byte;
				else if (s == 1) size = Size.Word;
				else if (s == 2) size = Size.Long;
				Append(size);
				//add

				int Xn = (type >> 9) & 7;

				if ((type & 0b1_00_000_000) != 0)
				{
					Append($"d{Xn},");
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
				}
				else
				{
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
					Append($",d{Xn}");
				}
			}
		}

		private void t_twelve(int type)
		{
			if ((type & 0b111_000000) == 0b011_000000) mulu(type);
			else if ((type & 0b111_000000) == 0b111_000000) muls(type);
			else if ((type & 0b11111_0000) == 0b10000_0000) abcd(type);
			else if ((type & 0b100110000) == 0b10000_0000) exg(type);
			else and(type);
		}

		private void and(int type)
		{
			Append("and");

			Size size = getSize(type);

			int Xn = (type >> 9) & 7;
			if ((type & 0b1_00_000000) != 0)
			{
				//R->M
				Append($"d{Xn},");
				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);
			}
			else
			{
				//M-R
				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);

				Append($",d{Xn}");
			}
		}

		private void exg(int type)
		{
			Append("exg");

			int Yn = type & 7;
			int Xn = (type >> 9) & 7;
			int mode = (type >> 3) & 0x1f;

			switch (mode)
			{
				case 0b01000://DD
					Append($" d{Xn},d{Yn}"); break;
				case 0b01001://AA
					Append($" a{Xn},a{Yn}"); break;
				case 0b10001://DA
					Append($" d{Xn},a{Yn}"); break;
				default:
					Append(" unknown_mode"); break;
			}
		}

		private void abcd(int type)
		{
			Append("abcd ");

			//abcd
			int Xn = (type >> 9) & 7;

			if ((type & 0b1_000) != 0)
			{
				//M->M
				Append($"-(a{Xn}),");
				type ^= 0b101_000;
				uint ea = fetchEA(type);
			}
			else
			{
				uint ea = fetchEA(type);
				Append($",d{Xn}");
			}
		}

		private void muls(int type)
		{
			int Xn = (type >> 9) & 7;
			Append($"muls.w ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append($",d{Xn}");
		}

		private void mulu(int type)
		{
			int Xn = (type >> 9) & 7;
			Append($"mulu.w ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append($",d{Xn}");
		}

		private void t_eleven(int type)
		{
			int op = (type & 0b111_000_000) >> 6;
			if (op == 0b011 || op == 0b111) cmpa(type);
			else if ((op & 0b100) == 0) cmp(type);
			else if (((type >> 3) & 7) == 0b001) cmpm(type);
			else eor(type);
		}

		private void eor(int type)
		{
			Append("eor");
			Size size = getSize(type);

			int Xn = (type >> 9) & 7;
			if ((type & 0b1_00_000000) != 0)
			{
				//R->M
				Append($"d{Xn},");
				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);
			}
			else
			{
				//M-R
				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);

				Append($",d{Xn}");
			}
		}

		private void cmpm(int type)
		{
			Append("cmpm");

			Size size = getSize(type);

			int Xn = type & 7;
			int Ax = (type>>9) & 7;
			Append($"(a{Xn})+,(a{Ax})+");
		}

		private void cmp(int type)
		{
			Append("cmp");
			Size size = getSize(type);
			uint ea = fetchEA(type);
			uint op0 = fetchOp(type, ea, size);
			Append(",");
			type = swizzle(type) & 7;
			ea = fetchEA(type);
			uint op1 = fetchOp(type, ea, size);
		}

		private void cmpa(int type)
		{
			if ((type & 0b1_00_000_000) != 0)
			{
				Append("cmpa");
				Append(Size.Long);
				uint ea = fetchEA(type);
				uint op0 = fetchOp(type, ea, Size.Long);
				Append(",");
				type = (swizzle(type) & 7) | 8;
				ea = fetchEA(type);
				uint op1 = fetchOp(type, ea, Size.Long);
			}
			else
			{
				Append("cmpa");
				Append(Size.Word);
				uint ea = fetchEA(type);
				uint op0 = fetchOp(type, ea, Size.Word);
				Append(",");
				type = (swizzle(type) & 7) | 8;
				ea = fetchEA(type);
				uint op1 = fetchOp(type, ea, Size.Word);
			}
		}

		private void t_nine(int type)
		{
			//sub

			int s = (type >> 6) & 3;
			Size size = 0;
			if (s == 3)
			{
				Append("suba");
				//suba
				if ((type & 0b1_00_000_000) != 0)
					size = Size.Long;
				else
					size = Size.Word;

				Append(size);

				int Xn = (type >> 9) & 7;

				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);

				Append($",a{Xn}");
			}
			else if ((type & 0b1_00_110_000) == 0b1_00_000_000)
			{
				Append("subx");

				if (s == 0) size = Size.Byte;
				else if (s == 1) size = Size.Word;
				else if (s == 2) size = Size.Long;
				Append(size);
				//subx
				int Xn = (type >> 9) & 7;

				if ((type & 0b1_000) != 0)
				{
					//M->M
					Append($"-(a{Xn}),");
					type ^= 0b101_000;
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
				}
				else
				{
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
					Append($",d{Xn}");
				}
			}
			else
			{
				Append("sub");
				if (s == 0) size = Size.Byte;
				else if (s == 1) size = Size.Word;
				else if (s == 2) size = Size.Long;
				Append(size);
				//sub

				int Xn = (type >> 9) & 7;

				if ((type & 0b1_00_000_000) != 0)
				{
					Append($"d{Xn},");
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
				}
				else
				{
					uint ea = fetchEA(type);
					uint op = fetchOp(type, ea, size);
					Append($",d{Xn}");
				}
			}

		}

		private void t_eight(int type)
		{
			if ((type & 0b111_000000) == 0b011_000000) divu(type);
			else if ((type & 0b111_000000) == 0b111_000000) divs(type);
			else if ((type & 0b11111_0000) == 0b10000_0000) sbcd(type);
			else or(type);
		}

		private void or(int type)
		{
			Append("or");

			Size size = getSize(type);

			int Xn = (type >> 9) & 7;
			if ((type & 0b1_00_000000) != 0)
			{
				//R->M
				Append($"d{Xn},");
				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);
			}
			else
			{
				//M-R
				uint ea = fetchEA(type);
				uint op = fetchOp(type, ea, size);

				Append($",d{Xn}");
			}
		}

		private void sbcd(int type)
		{
			Append("sbcd ");
			
			//sbcd
			int Xn = (type >> 9) & 7;

			if ((type & 0b1_000) != 0)
			{
				//M->M
				Append($"-(a{Xn}),");
				type ^= 0b101_000;
				uint ea = fetchEA(type);
			}
			else
			{
				uint ea = fetchEA(type);
				Append($",d{Xn}");
			}
		}

		private void divs(int type)
		{
			int Xn = (type >> 9) & 7;
			Append($"divs.w ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append($",d{Xn}");
		}

		private void divu(int type)
		{
			int Xn = (type >> 9) & 7;
			Append($"divu.w ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append($",d{Xn}");
		}

		private void t_seven(int type)
		{
			if (((type >> 16) & 1) == 0)
			{
				//moveq
				int Xn = (type >> 9) & 7;
				sbyte imm8 = (sbyte)(type & 0xff);
				Append($"moveq #{imm8},d{Xn}");
			}
			else
			{
				Append($"unknown_instruction_{type:X4}");
			}
		}

		private void t_six(int type)
		{
			int cond = (type >> 8) & 0xf;

			if (cond == 0)
				bra(type);

			else if (cond == 1)
				bsr(type);

			else
			{
				Append("b");
				dasm.type = M_TYPE.M_Bcc;
				switch (cond)
				{
					case 2:
						hi();
						break;
					case 3:
						ls();
						break;
					case 4:
						cc();
						break;
					case 5:
						cs();
						break;
					case 6:
						ne();
						break;
					case 7:
						eq();
						break;
					case 8:
						vc();
						break;
					case 9:
						vs();
						break;
					case 10:
						pl();
						break;
					case 11:
						mi();
						break;
					case 12:
						ge();
						break;
					case 13:
						lt();
						break;
					case 14:
						gt();
						break;
					case 15:
						le();
						break;
				}
				bra2(type);
			}
		}

		//file:///C:/source/programming/Amiga/M68000PRM.pdf
		//3-19
		private bool le()
		{
			Append("le");
			return false;
		}

		private bool gt()
		{
			Append("gt");
			return false;
		}

		private bool lt()
		{
			Append("lt");
			return false;
		}

		private bool ge()
		{
			Append("ge");
			return false;
		}

		private bool mi()
		{
			Append("mi");
			return false;
		}

		private bool pl()
		{
			Append("pl");
			return false;
		}

		private bool vs()
		{
			Append("vs");
			return false;
		}

		private bool vc()
		{
			Append("vc");
			return false;
		}

		private bool eq()
		{
			Append("eq");
			return false;
		}

		private bool ne()
		{
			Append("ne");
			return false;
		}

		private bool cs()
		{
			Append("cs");
			return false;
		}

		private bool cc()
		{
			Append("cc");
			return false;
		}

		private bool ls()
		{
			Append("ls");
			return false;
		}

		private bool hi()
		{
			Append("hi");
			return false;
		}

		private void bsr(int type)
		{
			Append("bsr");
			dasm.type = M_TYPE.M_BSR;
			bra2(type);
		}

		private void bra(int type)
		{
			Append("bra");
			dasm.type = M_TYPE.M_BRA;
			bra2(type);
		}

		private void bra2(int type)
		{
			Size size = Size.Byte;
			uint bas = pc;
			uint disp = (uint)(sbyte)(type & 0xff);
			if (disp == 0) {disp = (uint)(short)read16(pc); pc+=2; size = Size.Word; }
			else if (disp == 0xffffffff) {disp = read32(pc); pc += 4; size = Size.Long; }
			disp += address+2;
			dasm.ea = disp;
			AppendBcc(size);
			Append($"#{fmtX8(disp)}");
		}

		private void t_five(int type)
		{
			if ((type & 0b111_000000) == 0b000_000000) addqb(type);
			else if ((type & 0b111_000000) == 0b001_000000) addqw(type);
			else if ((type & 0b111_000000) == 0b010_000000) addql(type);
			else if ((type & 0b111_000000) == 0b100_000000) subqb(type);
			else if ((type & 0b111_000000) == 0b101_000000) subqw(type);
			else if ((type & 0b111_000000) == 0b110_000000) subql(type);
			else if ((type & 0b11111000) == 0b11001000) dbcc(type);
			else if ((type & 0b11000000) == 0b11000000) scc(type);
		}

		private void scc(int type)
		{
			Append("s");
			int cond = (type >> 8) & 0xf;
			switch (cond)
			{
				case 0:
					Append("t");
					break;
				case 1:
					Append("f");
					break;
				case 2:
					hi();
					break;
				case 3:
					ls();
					break;
				case 4:
					cc();
					break;
				case 5:
					cs();
					break;
				case 6:
					ne();
					break;
				case 7:
					eq();
					break;
				case 8:
					vc();
					break;
				case 9:
					vs();
					break;
				case 10:
					pl();
					break;
				case 11:
					mi();
					break;
				case 12:
					ge();
					break;
				case 13:
					lt();
					break;
				case 14:
					gt();
					break;
				case 15:
					le();
					break;
			}
			Append(" ");
			uint ea = fetchEA(type);
		}

		private void dbcc(int type)
		{
			int Xn = type & 7;

			uint target = (address+2) + (uint)(short)read16(pc);
			pc += 2;

			Append($"db");
			dasm.type = M_TYPE.M_DBcc;
			dasm.ea = target;

			int cond = (type >> 8) & 0xf;
			switch (cond)
			{
				case 0:
					Append("t");
					break;
				case 1:
					Append("ra");
					break;
				case 2:
					hi();
					break;
				case 3:
					ls();
					break;
				case 4:
					cc();
					break;
				case 5:
					cs();
					break;
				case 6:
					ne();
					break;
				case 7:
					eq();
					break;
				case 8:
					vc();
					break;
				case 9:
					vs();
					break;
				case 10:
					pl();
					break;
				case 11:
					mi();
					break;
				case 12:
					ge();
					break;
				case 13:
					lt();
					break;
				case 14:
					gt();
					break;
				case 15:
					le();
					break;
			}
			Append($" d{Xn},#{fmtX8(target)}(pc)");

		}

		private void subql(int type)
		{
			uint imm = (uint)((type >> 9) & 7);
			if (imm == 0) imm = 8;
			Append($"subq.l #{imm},");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Long);
		}

		private void subqw(int type)
		{
			uint imm = (uint)((type >> 9) & 7);
			if (imm == 0) imm = 8;
			Append($"subq.w #{imm},");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
		}

		private void subqb(int type)
		{
			uint imm = (uint)((type >> 9) & 7);
			if (imm == 0) imm = 8;
			Append($"subq.b #{imm},");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Byte);
		}

		private void addql(int type)
		{
			uint imm = (uint)((type >> 9) & 7);
			if (imm == 0) imm = 8;
			Append($"addq.l #{imm},");

			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Long);
		}

		private void addqw(int type)
		{
			uint imm = (uint)((type >> 9) & 7);
			if (imm == 0) imm = 8;
			Append($"addq.w #{imm},");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
		}

		private void addqb(int type)
		{
			uint imm = (uint)((type >> 9) & 7);
			if (imm == 0) imm = 8;
			Append($"addq.b #{imm},");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Byte);
		}

		private void t_four(int type)
		{
			int subins = (int)(type & 0x0fff);

			switch (subins)
			{
				case 0b1110_0111_0011:
					rte(type);
					break;
				case 0b1110_0111_0101:
					rts(type);
					break;
				case 0b1110_0111_0110:
					trapv(type);
					break;
				case 0b1110_0111_0111:
					rtr(type);
					break;
				case 0b1110_0111_0010:
					stop(type);
					break;
				case 0b1110_0111_0001:
					nop(type);
					break;
				case 0b1110_0111_0000:
					reset(type);
					break;
				case 0b1010_1111_1100:
					illegal(type);
					break;
				default:
					if ((subins & 0b1111_1100_0000) == 0b111010000000)
						jsr(type);
					else if ((subins & 0b1111_1100_0000) == 0b111011000000)
						jmp(type);
					else if ((subins & 0b111110111000) == 0b100010000000)
						ext(type);
					else if ((subins & 0b1011_1000_0000) == 0b100010000000)
						movem(type);
					else if ((subins & 0b0001_1100_0000) == 0b000111000000)
						lea(type);
					else if ((subins & 0b0001_1100_0000) == 0b000110000000)
						chk(type);
					else if ((subins & 0b1111_1111_0000) == 0b1110_0110_0000)
						moveusp(type);
					else if ((subins & 0b1111_1111_1000) == 0b1110_0101_1000)
						unlk(type);
					else if ((subins & 0b1111_1111_1000) == 0b1110_0101_0000)
						link(type);
					else if ((subins & 0b1111_1111_0000) == 0b1110_0100_0000)
						trap(type);
					else if ((subins & 0b111111_000000) == 0b000011_000000)
						movefromsr(type);
					else if ((subins & 0b111111_000000) == 0b010011_000000)
						movetoccr(type);
					else if ((subins & 0b111111_000000) == 0b011011_000000)
						movetosr(type);
					else if ((subins & 0b111111000000) == 0b100000000000)
						nbcd(type);
					else if ((subins & 0b111111111000) == 0b100001000000)
						swap(type);
					else if ((subins & 0b111111000000) == 0b100001000000)
						pea(type);
					else if ((subins & 0b1111_00000000) == 0b0000_00000000)
						negx(type);
					else if ((subins & 0b1111_00000000) == 0b0010_00000000)
						clr(type);
					else if ((subins & 0b1111_00000000) == 0b0100_00000000)
						neg(type);
					else if ((subins & 0b1111_00000000) == 0b0110_00000000)
						not(type);
					else if ((subins & 0b1111_11_000000) == 0b1010_11_000000)
						tas(type);
					else if ((subins & 0b1111_00000000) == 0b1010_00000000)
						tst(type);
					else if ((subins & 0b1111_1111_1110) == 0b1110_0111_1010)
						movec(type);
					else
						Append($"unknown_instruction_{type:X4}");
					break;
			}
		}

		private void movec(int type)
		{
			Dictionary<uint, string> crs = new Dictionary<uint, string>
			{
				{0x000, "SFC"},//>=68010
				{0x001, "DFC"},
				{0x800, "USP"},
				{0x801, "VBR"},
				{0x002, "CACR"},//>=68020
				{0x802, "CAAR"},
				{0x803, "MSP"},
				{0x804, "ISP"},
				{0x003, "TC"},//>= 68040
				{0x004, "ITT0"},
				{0x005, "ITT1"},
				{0x006, "DTT0"},
				{0x007, "DTT1"},
				{0x805, "MMUSR"},
				{0x806, "URP"},
				{0x807, "SRP"},
				//{0x004, "IACR0"},//68EC040 only
				//{0x005, "IACR1"},
				//{0x006, "DACR1"},
				//{0x007, "DACR2"}
			};

			Append("movec ");
			uint imm = fetchOpSize(pc,Size.Word);pc+=2;
			uint reg = (imm>>12)&7;
			uint cw = imm & 0x0fff;
			if ((type & 1) == 0)
			{
				//CR->reg
				if (!crs.TryGetValue(cw, out string cwstring))
					cwstring = "unknown_CR";
				Append($"{cwstring},");
				if ((imm & 0x8000)!=0)
					Append($"a{reg}");
				else

					Append($"d{reg}");
			}
			else
			{
				//reg->CR
				if ((imm & 0x8000) != 0)
					Append($"a{reg},");
				else
					Append($"d{reg},");
				if (!crs.TryGetValue(cw, out string cwstring))
					cwstring = "unknown_CR";
				Append($"{cwstring}");
			}
		}

		private void tst(int type)
		{
			Append("tst");
			Size size = getSize(type);
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void tas(int type)
		{
			Append("tas ");
			Size size = Size.Byte;
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void not(int type)
		{
			Append("not");
			Size size = getSize(type);
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void neg(int type)
		{
			Append("neg");
			Size size = getSize(type);
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void clr(int type)
		{
			Append("clr");
			Size size = getSize(type);
			uint ea = fetchEA(type);
		}

		private void negx(int type)
		{
			Append("negx");
			Size size = getSize(type);
			uint ea = fetchEA(type);
		}

		private void pea(int type)
		{
			Append("pea ");
			uint ea = fetchEA(type);
		}

		private void swap(int type)
		{
			int Xn = type & 7;
			Append($"swap d{Xn}");
		}

		private void nbcd(int type)
		{
			Append("nbcd ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Byte);
		}

		private void ext(int type)
		{
			int Xn = type & 7;
			int mode = (type >> 6) & 7;
			switch (mode)
			{
				case 0b010:
					Append($"ext.w d{Xn}");
					break;
				case 0b011:
					Append($"ext.l d{Xn}");
					break;
				case 0b111:
					Append($"extb.l d{Xn}");
					break;
				default:
					Append($"unknown_instruction_{type:X4}");
					break;
			}
		}

		private void movetosr(int type)
		{
			Append("move.w ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append(",sr");
		}

		private void movetoccr(int type)
		{
			Append("move.w ");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append(",ccr");
		}

		private void movefromsr(int type)
		{
			Append("move.w sr,");
			uint ea = fetchEA(type);
		}

		private void illegal(int type)
		{
			Append("illegal");
		}

		private void trap(int type)
		{
			uint vector = (uint)(type & 0xf);
			Append($"trap #{vector}");
		}

		private void link(int type)
		{
			int An = type & 7;
			short imm16 = (short)read16(pc); pc += 2;
			Append($"link a{An},#${(ushort)imm16:X4}");
		}

		private void unlk(int type)
		{
			int An = type & 7;
			Append($"unlk a{An}");
		}

		private void moveusp(int type)
		{
			int An = type & 7;
			if ((type & 0b1000) != 0)
				Append($"move usp,a{An}");
			else
				Append($"move a{An},usp");
		}

		private void reset(int type)
		{
			Append("reset");
		}

		private void nop(int type)
		{
			Append("nop");
		}

		private void stop(int type)
		{
			Append("stop ");
			fetchImm(Size.Word);
		}

		private void t_zero(int type)
		{
			if (((type >> 8) & 1) == 0 && (type >> 12) == 0)
			{
				int op = (type >> 9) & 7;
				switch (op)
				{
					case 0:
						ori(type);
						break;
					case 1:
						andi(type);
						break;
					case 2:
						subi(type);
						break;
					case 3:
						addi(type);
						break;
					case 4:
						bit(type);
						break;
					case 5:
						eori(type);
						break;
					case 6:
						cmpi(type);
						break;
					default:
						Append($"unknown_instruction_{type:X4}");
						break;
				}
			}
			else
			{
				int op = (type >> 12) & 3;
				switch (op)
				{
					case 0://bit or movep
						if (((type >> 3) & 7) == 0b001)
							movep(type);
						else
							bit(type);
						break;
					case 1://move byte
						moveb(type);
						break;
					case 3://move word
						movew(type);
						break;
					case 2://move long
						movel(type);
						break;
				}
			}
		}

		private void movep(int type)
		{
			Append("movep");

			Size size;
			int Xn = (type >> 9) & 7;

			if ((type & 0xb1_000000) != 0)
				size = Size.Long;
			else
				size = Size.Word;

			Append(size);

			if ((type & 0xb10000000) == 0)
			{
				fetchEA((type & 7) | 0b101 << 3);
				Append(",");
				Append($"d{Xn}");
			}
			else
			{
				Append($"d{Xn}");
				Append(",");
				fetchEA((type & 7) | 0b101 << 3);
			}
		}

		private int swizzle(int type)
		{
			//change a MOVE destination EA to look like a source one.
			return ((type >> 9) & 0b000111) | ((type >> 3) & 0b111000);
		}

		private void movel(int type)
		{
			Append("move");
			if (((type>>6)&0b111) == 0b001) Append("a");
			Append(Size.Long);

			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Long);
			Append(",");
			type = swizzle(type);
			ea = fetchEA(type);

		}

		private void movew(int type)
		{
			Append("move");
			if (((type >> 6) & 0b111) == 0b001) Append("a");
			Append(Size.Word);

			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Word);
			Append(",");
			type = swizzle(type);
			ea = fetchEA(type);
		}

		private void moveb(int type)
		{
			Append("move");
			if (((type >> 6) & 0b111) == 0b001) Append("a");
			Append(Size.Byte);

			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, Size.Byte);
			Append(",");

			type = swizzle(type);
			ea = fetchEA(type);
		}

		private void cmpi(int type)
		{
			Append("cmpi");
			Size size = getSize(type);
			uint imm = fetchImm(size);
			Append(",");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void eori(int type)
		{
			Append("eori");
			Size size = getSize(type);

			if (((type & 0b111111) == 0b111100) && size == Size.Byte)
			{
				uint easr = fetchEA(type);
				fetchOp(type, easr, size);
				Append(",ccr");
				return;
			}
			else if (((type & 0b111111) == 0b111100) && size == Size.Word)
			{
				uint easr = fetchEA(type);
				fetchOp(type, easr, size);
				Append(",sr");
				return;
			}
			uint imm = fetchImm(size);
			Append(",");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void bit(int type)
		{
			Size size;

			int op = (type >> 6) & 3;

			switch (op)
			{
				case 0://btst
					Append("btst");
					break;
				case 1://bchg
					Append("bchg");
					break;
				case 2://bclr
					Append("bclr");
					break;
				case 3://bset
					Append("bset");
					break;
			}

			//if target is a register, then it's a long else it's a byte
			if (((type & 0b111000) >> 3) == 0)
				size = Size.Long;
			else
				size = Size.Byte;

			Append(size);

			uint bit;
			if ((type & 0b100000000) != 0)
			{
				//bit number is in Xn
				int Xn = (type >> 9) & 7;
				Append($"d{Xn}");
			}
			else
			{
				//bit number is in immediate byte following
				ushort imm16 = read16(pc);
				bit = (uint)(imm16 & 0xff); pc += 2;
				Append($"#{bit}");
			}

			Append(",");

			uint ea = fetchEA(type);
			uint op0 = fetchOp(type, ea, size);


		}

		private void addi(int type)
		{
			Append($"addi");
			Size size = getSize(type);
			uint imm = fetchImm(size);
			Append(",");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void subi(int type)
		{
			Append($"subi");
			Size size = getSize(type);
			uint imm = fetchImm(size);
			Append(",");
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
		}

		private void andi(int type)
		{
			Append($"andi");
			Size size = getSize(type);

			if (((type & 0b111111) == 0b111100) && size == Size.Byte)
			{
				uint easr = fetchEA(type);
				fetchOp(type, easr, size);
				Append(",ccr");
				return;
			}
			else if (((type & 0b111111) == 0b111100) && size == Size.Word)
			{
				uint easr = fetchEA(type);
				fetchOp(type, easr, size);
				Append(",sr");
				return;
			}

			uint imm = fetchImm(size);
			Append(",");

			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);

		}

		private void ori(int type)
		{
			Append($"ori");

			Size size = getSize(type);

			if (((type & 0b111111) == 0b111100) && size == Size.Byte)
			{
				uint easr = fetchEA(type);
				fetchOp(type, easr, size);
				Append(",ccr");
				return;
			}
			else if (((type&0b111111) == 0b111100) && size == Size.Word)
			{
				uint easr = fetchEA(type);
				fetchOp(type, easr, size);
				Append(",sr");
				return;
			}

			uint imm = fetchImm(size);
			Append(",");

			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);

		}

		private void chk(int type)
		{
			int Xn = (type >> 9) & 7;
			Append($"chk");
			Size size;
			if ((type & 0b11_0000000) == 0b11_0000000)
				size = Size.Word;
			else if ((type & 0b11_0000000) == 0b10_0000000)
				size = Size.Long;
			else
				size = Size.Extension;
			Append(size);
			uint ea = fetchEA(type);
			uint op = fetchOp(type, ea, size);
			Append($",d{Xn}");
		}

		private void lea(int type)
		{
			Append("lea ");
			fetchEA(type);
			int An = (type >> 9) & 7;
			Append($",a{An}");
		}

		private void movem(int type)
		{
			Append("movem");

			Size size;

			if ((type & 0b1000000) != 0)
				size = Size.Long;
			else
				size = Size.Word;

			Append(size);

			uint mask = read16(pc); pc +=2;

			if ((type & 0b1_0000_000000) != 0)
			{
				uint ea = fetchEA(type);
				Append(",");
				AppendRegSetDA(mask);
			}
			else
			{
				//R->M
				//if it's pre-decrement mode
				if ((type & 0b111_000) == 0b100_000)
					AppendRegSetAD(mask);
				else
					AppendRegSetDA(mask);
				Append(",");
				uint ea = fetchEA(type);
			}
		}

		private class RRange
		{
			private readonly char R;
			private readonly int S;
			private int E;

			public RRange(char r, int s)
			{
				R = r;
				S = s;
			}

			public void End(int e)
			{
				E = e;
			}

			public override string ToString()
			{
				if (S == E) return $"{R}{S}";
				if (S+1 == E) return $"{R}{S}/{R}{E}";//or $"{R}{S}-{R}{E}";
				return $"{R}{S}-{R}{E}";
			}
		}

		private void AppendRegSetAD(uint mask)
		{
			//reverse the bits
			uint flip = 0;
			for (int i = 0; i < 16; i++)
			{
				flip <<= 1;
				flip |= mask & 1;
				mask >>= 1;
			}
			AppendRegSetDA(flip);
		}

		private void AppendRegSetDA(uint mask)
		{
			var ranges = new List<RRange>();

			var inb = false;
			var reg = 'd';
			for (int j = 0; j <= 8; j += 8)
			{
				for (int i = 0; i < 8; i++)
				{
					bool bit = (mask & (1 << (i+j))) != 0;
					if (!inb && bit)
					{
						//going in
						inb = true;
						ranges.Add(new RRange(reg, i));
					}
					else if (inb && !bit)
					{
						//going out
						inb = false;
						ranges.Last().End(i - 1);
					}
				}
				if (inb) ranges.Last().End(7);
				inb = false;
				reg = 'a';
			}

			Append(string.Join('/', ranges.Select(x=>x.ToString())));
		}

		private void jmp(int type)
		{
			Append($"jmp ");
			dasm.type = M_TYPE.M_JMP;
			fetchEA(type);
		}

		private void jsr(int type)
		{
			Append($"jsr ");
			dasm.type = M_TYPE.M_JSR;
			fetchEA(type);
		}

		private void rtr(int type)
		{
			Append("rtr");
		}

		private void trapv(int type)
		{
			Append("trapv");
		}

		private void rts(int type)
		{
			Append("rts");
		}

		private void rte(int type)
		{
			Append("rte");
		}
	}
}
