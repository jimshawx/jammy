﻿/*
	Copyright 2020-2021 James Shaw. All Rights Reserved.
*/

namespace Jammy.Core.Types.Types
{
	public enum EmulationMode
	{
		Stopped,
		Step,
		Running,
		Exit,
		StepOut
	}
}
