﻿// ReSharper disable InconsistentNaming
/*
	Copyright 2020-2021 James Shaw. All Rights Reserved.
*/

namespace Jammy.Types
{
	public class ChipState
	{
		public ushort intena { get; set; }
		public ushort intreq { get; set; }
		public ushort dmacon { get; set; }
	}
}
